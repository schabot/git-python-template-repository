#!/usr/bin/env python3
# coding: utf-8

from sensors_stats.tools import stats_of


def test_equality():
    a = 42

    assert a == 42


def test_stats_of():
    numbers = (1, 2, 3, 4)

    stats = stats_of(numbers)

    assert stats == (1, 2.5, 4)
