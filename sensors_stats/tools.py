#!/usr/bin/env python3
# coding: utf-8

import csv
from statistics import mean


def load_csv_dict(filepath):
    data = {}
    with open(filepath) as fobj:
        reader = csv.DictReader(fobj, delimiter="\t")
        for record in reader:
            for key, value in record.items():
                data.setdefault(key, []).append(float(value))
    return data


def stats_of(numbers):
    return min(numbers), mean(numbers), max(numbers)


def stats_dict(data):
    result = {}
    for key, numbers in data.items():
        result[key] = stats_of(numbers)
    return result


def dump_csv_stats(data, filepath, overwrite=True, delimiter="\t"):
    with open(filepath, "w" if overwrite else "a") as fobj:
        writer = csv.writer(fobj, delimiter=delimiter)
        for key, values in data.items():
            writer.writerow([key, *values])


if __name__ == "__main__":
    data = load_csv_dict("./data/sensor.csv")
    stats_data = stats_dict(data)
    dump_csv_stats(stats_data, "stats.csv", overwrite=False)
