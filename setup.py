#!/usr/bin/env python3
# coding: utf-8

from setuptools import setup, find_packages

setup(
    name="Example",
    version="0.1",
    description="A description of my package",
    author="My Name",
    author_email="My email",
    url="https://example.fr/my-project",
    packages=find_packages(),
    install_requires=[],  # a list of requirements to be completed.
)
