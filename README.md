# git-python-template-repository

Hello, and welcome to this “template” repository.

This repository is almost empty, but contains a simple `.gitlab-ci.yml` that you
will have to complete. This file lists several tasks you can add to your
gitlab-ci.yml. Once you've completed the task, you can fulfill the corresponding
`[ ]` with an `x` to mark it as completed.

## Add your files

- [ ] Create a new folder named `sensors_stats` and copy/paste the code written
  for the `Sensors Stats` exercise into a file in this folder.
- [ ] Update the `setup.py` file to make it valid.

## Set up the CI

### The lint

- [ ] Update the `flake8` stage in the `.gitlab-ci.yml`to actually run flake8 on
  your code.
- [ ] Add a new job to the `lint` stage to check that the code is formatted with
  black.

### The test

- [ ] Add basic tests to your module in a `test` folder, make sure they succeed
  locally.
- [ ] Add a new stage `tests`

### Publish

- [ ] Add a new stage `publish` that will publish a new version of your module
  to the gitlab python package repository *on a new tag*. You can find
  interesting information in the
  [documentation](https://docs.gitlab.com/ee/user/packages/pypi_repository/#authenticate-with-a-ci-job-token).
